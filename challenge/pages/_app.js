import Layout from "@/components/layout/Layout";
import theme, { cacheRtl } from "@/mui/Theme";
import "@/styles/globals.css";
import { CacheProvider, ThemeProvider } from "@emotion/react";

export default function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <CacheProvider value={cacheRtl}>
        <Layout style={{ direction: "rtl" }}>
          <Component {...pageProps} />
        </Layout>
      </CacheProvider>
    </ThemeProvider>
  );
}
