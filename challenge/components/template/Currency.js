import { Box, Toolbar } from "@mui/material";
import Header from "../module/Header";
import TabComponent from "../module/TabComponent";

const drawerWidth = 300;

const Currency = ({ coins }) => {
  return (
    <Box
      sx={{
        flexGrow: 1,
        p: 3,
        width: { md: `calc(100% - ${drawerWidth}px)` },
      }}>
      <Toolbar
        sx={{
          display: {
            xs: "block",
            sm: "block",
            md: "none",
            lg: "none",
            xl: "none",
          },
        }}
      />
      <Header />
      <TabComponent coins={coins} />
    </Box>
  );
};

export default Currency;
