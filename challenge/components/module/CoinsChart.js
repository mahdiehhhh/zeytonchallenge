import axios from "axios";
import { useEffect, useRef } from "react";

const canvasWidth = 50;
const canvasHeight = 50;
const padding = 5;
const color = "#11BB69";

const CoinsChart = ({ coinId }) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://api.coingecko.com/api/v3/coins/${coinId}/market_chart/range?vs_currency=usd&from=1627747200&to=1630349200`
        );

        if (response.status === 200) {
          const { prices } = response.data;
          drawCoordinates(prices);
        } else {
          console.log("Failed to fetch coin data.");
        }
      } catch (error) {
        console.log("Error:", error);
      }
    };

    fetchData();
  }, [coinId]);

  const drawCoordinates = (data) => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    // Calculate the minimum and maximum prices for scaling the coordinates
    const prices = data.map((entry) => entry[1]);
    const minPrice = Math.min(...prices);
    const maxPrice = Math.max(...prices);

    // Clear the canvas before drawing
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);

    // Draw the coordinates on the canvas as a smaller line chart
    ctx.beginPath();
    for (let i = 0; i < data.length; i++) {
      const x = (i / (data.length - 1)) * (canvasWidth - padding * 2) + padding;
      const y =
        canvasHeight -
        ((data[i][1] - minPrice) / (maxPrice - minPrice)) *
          (canvasHeight - padding * 2) -
        padding;

      if (i === 0) {
        ctx.moveTo(x, y);
      } else {
        ctx.lineTo(x, y);
      }
    }

    ctx.strokeStyle = color;
    ctx.lineWidth = 1;
    ctx.stroke();
  };

  return (
    <canvas
      ref={canvasRef}
      width={canvasWidth}
      height={canvasHeight}
    />
  );
};

export default CoinsChart;
