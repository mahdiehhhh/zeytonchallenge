import {
  IconButton,
  Paper,
  Rating,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useState } from "react";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import Image from "next/image";
import CoinsChart from "./CoinsChart";

// Utility function to convert numbers to Persian format
export function convertToPersianNumber(number) {
  const persianDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  return String(number).replace(
    /[0-9]/g,
    (match) => persianDigits[Number(match)]
  );
}

const columns = [
  { id: "نوع", label: "نوع", minWidth: 70 },
  { id: "قیمت خرید", label: "قیمت خرید", minWidth: 70 },
  { id: "قیمت فروش", label: "قیمت فروش", minWidth: 70 },
  { id: "تغییرات", label: "تغییرات", minWidth: 70 },
  { id: "مقدار تغییرات", label: "مقدار تغییرات", minWidth: 70 },
  {
    id: "نمودار تغییرات (یک ماه اخیر)",
    label: " نمودار تغییرات (یک ماه اخیر)",
    minWidth: 170,
  },
];

const CustomPagination = ({ page, setPage, pageCount }) => {
  const handlePrevPage = () => {
    setPage((prevPage) => Math.max(prevPage - 1, 0));
  };

  const handleNextPage = () => {
    setPage((prevPage) => Math.min(prevPage + 1, pageCount - 1));
  };

  return (
    <Stack
      direction='row'
      alignItems='center'
      justifyContent='center'
      m={3}>
      <IconButton
        onClick={handleNextPage}
        disabled={page === pageCount - 1}>
        <ChevronRightIcon />
      </IconButton>

      <Typography variant='body1'>
        صفحه {page + 1} از {pageCount}
      </Typography>
      <IconButton
        onClick={handlePrevPage}
        disabled={page === 0}>
        <ChevronLeftIcon />
      </IconButton>
    </Stack>
  );
};

const rowsPerPage = 10;
const TableComponent = ({ coins }) => {
  console.log(coins, "coinscoinscoins");
  const [page, setPage] = useState(0);
  const initialValue = 0;

  return (
    <Stack>
      <Paper sx={{ width: "100%", overflow: "hidden", boxShadow: "none" }}>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    sx={{ textAlign: "center" }}>
                    <Typography sx={{ display: "flex", alignItems: "center" }}>
                      {column.label}
                      <Image
                        src='/assets/image/SortArrow.svg'
                        alt='SortArrow'
                        width={15}
                        height={15}
                      />
                    </Typography>
                  </TableCell>
                ))}
                <TableCell></TableCell> {/* Empty cell for sorting arrow */}
              </TableRow>
            </TableHead>
            <TableBody>
              {coins
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => (
                  <TableRow key={row.id}>
                    <TableCell>
                      <Stack
                        direction='row'
                        alignItems='center'
                        spacing={1}>
                        <Rating
                          name='customized-10'
                          value={initialValue}
                          defaultValue={1}
                          max={1}
                          sx={{ paddingRight: "10px" }}
                        />
                        <Image
                          src={row.image}
                          alt={row.name}
                          width={20}
                          height={20}
                        />
                        <Typography sx={{ paddingLeft: "10px" }}>
                          {row.name === "Bitcoin"
                            ? "بیت کوین"
                            : row.name === "Ethereum"
                            ? "اتریوم"
                            : row.name === "Tether"
                            ? "تتر"
                            : row.name === "BNB"
                            ? "بی ان بی"
                            : row.name === "XRP"
                            ? "ایکس آر پی"
                            : row.name === "USD Coin"
                            ? "یو اس دی کوین"
                            : row.name === "Lido Staked Ether"
                            ? "لیدو استیکد ایدر"
                            : row.name === "Dogecoin"
                            ? "دوج کوین"
                            : row.name === "Cardano"
                            ? "کاردونو"
                            : row.name === "Solana"
                            ? "سولانا"
                            : row.name}
                        </Typography>
                      </Stack>
                    </TableCell>
                    <TableCell>
                      {convertToPersianNumber(row.low_24h.toFixed(2))}
                    </TableCell>
                    <TableCell>
                      {convertToPersianNumber(row.current_price.toFixed(2))}
                    </TableCell>
                    <TableCell>
                      {convertToPersianNumber(row.price_change_24h.toFixed(2))}
                    </TableCell>
                    <TableCell>
                      {convertToPersianNumber(
                        row.price_change_percentage_24h.toFixed(2)
                      )}
                    </TableCell>
                    <TableCell
                      sx={{ display: "flex", justifyContent: "center" }}>
                      <CoinsChart coinId={row.id} />
                    </TableCell>
                    <TableCell>
                      <Stack
                        direction='row'
                        justifyContent='space-between'>
                        {row.chart}
                        <Image
                          src='/assets/image/Group.svg'
                          alt='Group'
                          width={4}
                          height={16}
                        />
                      </Stack>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          <CustomPagination
            page={page}
            setPage={setPage}
            pageCount={Math.ceil(coins.length / rowsPerPage)}
          />
        </TableContainer>
      </Paper>
    </Stack>
  );
};

export default TableComponent;
