import {
  FormControl,
  Grid,
  InputAdornment,
  OutlinedInput,
  Stack,
  Typography,
} from "@mui/material";
import Image from "next/image";

const Header = () => {
  return (
    <Grid
      container
      spacing={{ xs: 2, md: 3 }}
      columns={{ xs: 4, sm: 8, md: 12 }}>
      <Grid
        item
        xs={4}
        sm={4}
        md={6}>
        <Stack
          direction='row'
          alignItems='center'>
          <Typography
            variant='h6'
            sx={{ fontWeight: "bold" }}>
            مهرناز
          </Typography>
          <Typography variant='body2'> عزیز، سلام!</Typography>
        </Stack>
      </Grid>

      <Grid
        item
        xs={4}
        sm={4}
        md={6}>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='flex-end'
          gap={3}>
          <FormControl
            sx={{
              border: "none",
              "& fieldset": { border: "none" },
            }}
            variant='filled'>
            <OutlinedInput
              sx={{
                height: "48px",
                border: "none",
                outline: "none",
                background: "#ffffff",
                borderRadius: "12px",
              }}
              placeholder='جست و جو'
              id='outlined-adornment-weight'
              startAdornment={
                <InputAdornment position='end'>
                  <Image
                    src='/assets/image/Search.svg'
                    alt='Search'
                    width={24}
                    height={24}
                    style={{ paddingLeft: "10px" }}
                  />
                </InputAdornment>
              }
              aria-describedby='outlined-weight-helper-text'
              inputProps={{
                "aria-label": "weight",
              }}
            />
          </FormControl>
          <Image
            src='/assets/image/Question.svg'
            alt='Question'
            width={24}
            height={24}
          />
          <Image
            src='/assets/image/NotifBell.svg'
            alt='NotifBell'
            width={24}
            height={24}
          />

          <Image
            src='/assets/image/Ellipse 1944.svg'
            alt='Profile'
            width={48}
            height={48}
          />
        </Stack>
      </Grid>
    </Grid>
  );
};

export default Header;
