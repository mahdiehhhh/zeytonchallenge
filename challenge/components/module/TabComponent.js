import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { useState } from "react";
import Image from "next/image";
import dynamic from "next/dynamic";

const TableComponent = dynamic(() => import("./TableComponent"), {
  ssr: false,
});

const tabPanelStyle = {
  backgroundColor: "#ffffff",
  borderRadius: "24px",
};

const filterImageStyle = {
  background: "#ffff",
  width: "35px",
  height: "35px",
  padding: "8px",
  borderRadius: "7px",
};

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      style={tabPanelStyle}
      {...other}>
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function IndexProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function TabComponent({ coins }) {
  console.log("coin", coins);
  const [value, setValue] = useState(0);

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%", marginTop: "100px" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: { xs: "flex-end" },
            alignItems: "center",
            position: { xs: "unset", md: "absolute", lg: "absolute" },
            right: "40px",
            marginBottom: { xs: "25px", md: "0px", lg: "0px" },
            gap: 2,
          }}>
          <Image
            src='/assets/image/Filter.svg'
            alt='filter'
            width={24}
            height={24}
            style={filterImageStyle}
          />

          <Image
            src='/assets/image/Favorite.svg'
            alt='Favorite'
            width={24}
            height={24}
            style={filterImageStyle}
          />
        </Box>
        <Tabs
          value={value}
          indicatorColor='bgWhite'
          onChange={handleChange}
          aria-label='basic tabs example'>
          <Tab
            label='ارزها'
            {...IndexProps(0)}
          />
          <Tab
            label='رمزارزها'
            {...IndexProps(1)}
          />
        </Tabs>
      </Box>
      <CustomTabPanel
        value={value}
        index={0}>
        <TableComponent coins={coins} />
      </CustomTabPanel>
      <CustomTabPanel
        value={value}
        index={1}>
        Item Two
      </CustomTabPanel>
    </Box>
  );
}
