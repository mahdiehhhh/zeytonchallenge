import {
  AppBar,
  Box,
  CssBaseline,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import MenuIcon from "@mui/icons-material/Menu";
const drawerWidth = 300;

const SideNav = (props) => {
  const router = useRouter();
  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const items = [
    { title: "داشبورد", link: "/", icon: "/assets/image/Home.svg" },
    { title: "کیف پول", link: "/", icon: "/assets/image/Wallet.svg" },
    { title: "سفارشات", link: "/", icon: "/assets/image/Services.svg" },
    { title: "نرخ ارز", link: "/", icon: "/assets/image/Currency.svg" },
    { title: "پروفایل", link: "/", icon: "/assets/image/Profile.svg" },
    { title: "معرفی حساب بانکی", link: "/", icon: "/assets/image/Bank.svg" },
    { title: "تیکت", link: "/", icon: "/assets/image/Ticket.svg" },
    { title: "تنظیمات", link: "/", icon: "/assets/image/Setting.svg" },
    {
      title: "باشگاه مشتریان",
      link: "/",
      icon: "/assets/image/CustomerClub.svg",
    },
  ];

  const drawer = (
    <Box sx={{ height: "100%", paddingTop: "15px" }}>
      <List dense>
        <ListItem sx={{ marginBottom: "25px" }}>
          <Image
            src='/assets/image/ZimaLogo.svg'
            width={160}
            height={33}
            alt='logo'
          />
        </ListItem>
        {items.map((item, index) => (
          <ListItem
            key={index}
            disablePadding>
            <Link
              href={item.link}
              style={{
                width: "100%",
                color: item.link === router.pathname ? "#606060" : "#F44444",
                textDecoration: "none",
              }}>
              <ListItemButton sx={{ ":hover": { bgcolor: "backgroundGray" } }}>
                <ListItemIcon>
                  <Image
                    src={item.icon}
                    width={20}
                    height={20}
                    alt={item.title}
                  />
                </ListItemIcon>
                <ListItemText
                  sx={{
                    color:
                      item.title === "نرخ ارز" ? "primary.main" : "textColor",
                  }}>
                  <Box>
                    {item.title === "نرخ ارز" ? (
                      <Typography
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}>
                        {item.title}
                        <Image
                          src='/assets/image/DropDown.svg'
                          alt='DropDown'
                          width={10}
                          height={10}
                        />
                      </Typography>
                    ) : item.title === "پروفایل" ? (
                      <Typography
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}>
                        {item.title}
                        <Image
                          src='/assets/image/NotifNumberIcon.svg'
                          alt='NotifNumberIcon'
                          width={30}
                          height={30}
                        />
                      </Typography>
                    ) : (
                      <Typography>{item.title}</Typography>
                    )}
                  </Box>
                </ListItemText>
              </ListItemButton>
            </Link>
          </ListItem>
        ))}
        <ListItem sx={{ marginTop: "2rem" }}>
          <Image
            src='/assets/image/AuthenticationAlert.svg'
            alt='AuthenticationAlert'
            width={200}
            height={200}
          />
        </ListItem>
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Stack>
      <CssBaseline />
      <Stack display={{ xs: "block", sm: "none" }}>
        <AppBar
          position='fixed'
          sx={{
            width: { sm: `calc(100% - ${drawerWidth}px)` },
            ml: { sm: `${drawerWidth}px` },
          }}
          color='bgWhite'>
          <Toolbar>
            <IconButton
              color='primary.main'
              aria-label='open drawer'
              edge='start'
              onClick={handleDrawerToggle}
              sx={{ display: { sm: "none" } }}>
              <MenuIcon color='primary' />
            </IconButton>
            <Image
              src='/assets/image/ZimaLogo.svg'
              width={160}
              height={33}
              alt='logo'
            />
          </Toolbar>
        </AppBar>
      </Stack>
      <Box
        component='nav'
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label='mailbox folders'>
        <Drawer
          container={container}
          variant='temporary'
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}>
          {drawer}
        </Drawer>
        <Drawer
          variant='permanent'
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          color='#ff0000'
          open>
          {drawer}
        </Drawer>
      </Box>
    </Stack>
  );
};

export default SideNav;
