import { Box, Stack } from "@mui/material";
import SideNav from "../module/SideNav";

const Layout = ({ children }) => {
  return (
    <Box sx={{ display: "flex", direction: "rtl" }}>
      {children}
      <SideNav />
    </Box>
  );
};

export default Layout;
