import createCache from "@emotion/cache";
import { createTheme } from "@mui/material";
import { prefixer } from "stylis";
import rtlPlugin from "stylis-plugin-rtl";

const theme = createTheme({
  direction: "rtl",
  palette: {
    primary: { main: "#298FFE" },
    secondary: { main: "#F44444" },
    textColor: { main: "#606060" },
    newGreen: { main: "#11BB69" },
    backgroundGray: { main: "#f6f7fa" },
    bgWhite: { main: "#ffffff" },
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
    },
  },
});
export default theme;

export const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});
